class Machine {
    constructor() {
        this.turn = false
    }

        turnOn() {
            this.turn = true;
    }


        turnOf(){
            console.log('Device turned off');
        this.turn = false
        }

};

class HomeAppliance extends Machine{
    constructor() {
        super();
        this.plug = false;
    };

    plugIn() {
        this.plug = true;
        console.log('Device connected to the network');
    }

    turnOn() {
        super.turnOn();
        if (this.plug) {
            this.turn = true;
            console.log('Device turned on');
            console.log('Device successfully connected')
        }
    }


    plugOff() {
        this.plug = false;
        console.log('The Device is not connected to the network')
    }

};

class WashingMachine extends HomeAppliance {
    constructor() {
        super();

    }
    run() {
        if (this.plug === false &&this.turn === true) {
            console.log('At first connect to the network, then turn on');
        }
      else if (this.turn === false && this.plug === true) {
           console.log('Device was connecting successfully. You can turn on the machine')
       }
    }
};

class LightSource extends HomeAppliance {
    constructor() {
        super();
    }

    setLevel(level) {
        if (level > 0 && level < 100) {
            console.log('Light level is ' + level);
        } else {
            console.log('Please enter level from 1 to 100')
        }
    }

}


class AutoVehicle  extends Machine{
    constructor() {
        super();
        this.position = {
            x: 0,
            y: 0
        }
    }
    setPosition(x, y) {
        this.position.x = x;
        this.position.y = y;
        console.log('Position x : ' + x + ' Position y: ' + y);
    }
};

class Car extends AutoVehicle{
    constructor() {
        super();
        this.speed = 10;
    }
    setSpeed(number) {
        this.speed = number;
        console.log('Speed : ' + number)
    }

    run(x, y) {
        let interval = setInterval(() => {
            this.position.x += this.speed;
            this.position.y += this.speed;
            if (this.position.x >= x) this.position.x = x;
            if (this.position.y >= y) this.position.y = y;
            console.log('Position x : ' + this.position.x);
            console.log('Position y : ' + this.position.y);
            if (this.position.x === x && this.position.y === y) {
                clearInterval(interval);
                console.log('***** Finish *****')
            }
        }, 1000)
    }

}



